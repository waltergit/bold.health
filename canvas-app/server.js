const net = require("net");
const _ = require("underscore");
const canvasUtils = require("./utils/canvas-utils");
const Canvas = require("./components/Canvas");

//define host and port to run the server
const port = 8124;
const host = "127.0.0.1";

//Clients array
var clients = [];
//Create an instance of the server
const server = net.createServer(onClientConnection);
//Start listening with the server on given port and host.
server.listen(port, host, function () {
  console.log(`Server started on port ${port} at ${host}`);
});

//Declare connection listener function
function onClientConnection(socket) {
  var clientAddress = `${socket.remoteAddress}:${socket.remotePort}`,
    canvas;
  //Log when a client connnects.
  console.log(`${clientAddress} Connected`);

  //Handle multiple clients
  var client = clients.find((client) => client.address == clientAddress);
  if (client) {
    canvas = client.canvas;
  } else {
    //Initialize client canvas
    canvas = new Canvas(30, 30);
    clients.push({ address: clientAddress, canvas });
  }

  console.log("Client connect, count: ", clients.length);

  //Welcome message
  socket.write("hello\r\n");

  //Listen for data from the connected client.
  socket.on("data", function (data) {
    //Log data from the client
    console.log(`${clientAddress} Says : ${data} `);

    //Parse commands
    var parsedCommands = canvasUtils.commandParser(data.toString());

    //Action
    var results = canvasUtils.commandRunner(parsedCommands, canvas);

    //Write to socket
    results.data.forEach((result) => {
      socket.write(result + "\r\n");
    });

    if (results.quit) {
      var clientIndex = clients.findIndex(
        (client) => client.address == clientAddress
      );
      clients.splice(clientIndex, 1);
      socket.destroy();
    }
  });
  //Handle client connection termination.
  socket.on("close", function () {
    var clientIndex = clients.findIndex(
      (client) => client.address == clientAddress
    );
    clients.splice(clientIndex, 1);
    console.log(`${clientAddress} Terminated the connection`);
    console.log("Server client connected count: ", clients.length);
  });
  //Handle Client connection error.
  socket.on("error", function (error) {
    console.error(`${clientAddress} Connection Error ${error}`);
  });
}
