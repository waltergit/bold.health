const _ = require("underscore");
const { COMMANDS } = require("../constants");

const commandParser = (rawCommands) => {
  //Parsing
  var cleanedCommands = _.compact(
    rawCommands.split("\r\n").map((el) => el.trim())
  );

  //Split command/args
  var commands = cleanedCommands.map((el) => {
    var cmdArgs = _.compact(el.split(" "));
    var command = cmdArgs.shift();
    return {
      command: command,
      args: cmdArgs,
    };
  });
  //Remove unknown commands
  var commands = commands.filter((el) => COMMANDS.includes(el.command));
  return commands;
};

const commandRunner = (commands, canvas) => {
  var data = [];
  var quit = false;
  for (var i = 0; i < commands.length; i++) {
    var action = commands[i];
    if (action.command == "quit") {
      quit = true;
      break;
    }
    //Run command with argument(s)
    try {
      var result = canvas[action.command](...action.args);
      if (result) {
        data.push(result);
      }
    } catch (err) {
      console.error(err);
    }
  }
  return { data: _.flatten(data), quit };
};

module.exports = {
  commandParser,
  commandRunner,
};
