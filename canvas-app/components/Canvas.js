const { BRUSHES, BORDERS, DIRECTION_INCREMENTS } = require("../constants");

class Canvas {
  constructor(width, height, cursor = [15, 15], brush = BRUSHES.DRAW) {
    this._height = height;
    this._width = width;
    this._cells = [].concat(
      new Array(height).fill(0).map((el) => new Array(width).fill(" "))
    );
    this._direction = 0;
    this._cursor = cursor;
    this._brush = brush;
  }

  render() {
    var topBorder = [BORDERS.TL]
      .concat(new Array(this._width).fill(BORDERS.H))
      .concat([BORDERS.TR])
      .join("");
    var bottomBorder = [BORDERS.BL]
      .concat(new Array(this._width).fill(BORDERS.H))
      .concat([BORDERS.BR])
      .concat(["\r\n"])
      .join("");

    var lines = [topBorder];

    lines = lines.concat(
      this._cells.map((el) =>
        [BORDERS.V].concat(el).concat([BORDERS.V]).join("")
      )
    );
    lines.push(bottomBorder);
    return lines;
  }

  steps(n = 1) {
    for (var i = 0; i < n; i++) {
      switch (this._brush) {
        case BRUSHES.DRAW:
          this.drawCell(this._cursor);
          break;
        case BRUSHES.ERASER:
          this.erase(this._cursor);
          break;
        case BRUSHES.HOVER:
          break;
      }
      this.move();
    }
  }

  coord() {
    return [`(${this._cursor[0]},${this._cursor[1]})`];
  }

  move() {
    var newCursor = [
      this._cursor[0] + DIRECTION_INCREMENTS[this._direction][0],
      this._cursor[1] + DIRECTION_INCREMENTS[this._direction][1],
    ];
    if (
      newCursor[0] >= 0 &&
      newCursor[0] <= this._width &&
      newCursor[1] >= 0 &&
      newCursor[0] <= this._height
    ) {
      this._cursor = newCursor;
    }
  }

  drawCell(coords) {
    if (
      coords[0] >= 0 &&
      coords[0] < this._width &&
      coords[1] >= 0 &&
      coords[1] < this._height
    ) {
      this._cells[coords[1]][coords[0]] = "*";
    }
  }

  erase(coords) {
    if (
      coords[0] >= 0 &&
      coords[0] < this._width &&
      coords[1] >= 0 &&
      coords[1] < this._height
    ) {
      this._cells[coords[1]][coords[0]] = " ";
    }
  }

  clear() {
    this._cells = [].concat(
      new Array(this._height)
        .fill(0)
        .map((el) => new Array(this._width).fill(" "))
    );
  }

  left(n = 1) {
    var newDir = this._direction - (n % 8);
    this._direction = newDir >= 0 ? newDir : newDir + 8;
  }

  right(n = 1) {
    var newDir = this._direction + (n % 8);
    this._direction = newDir > 7 ? newDir - 8 : newDir;
  }

  brush() {
    return [this._brush];
  }

  hover() {
    this._brush = BRUSHES.HOVER;
  }

  draw() {
    this._brush = BRUSHES.DRAW;
  }

  eraser() {
    this._brush = BRUSHES.ERASER;
  }
}

module.exports = Canvas;
