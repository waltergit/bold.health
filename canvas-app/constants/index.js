const BRUSHES = {
  DRAW: "draw",
  ERASER: "eraser",
  HOVER: "hover",
};

const BORDERS = {
  TL: "╔",
  TR: "╗",
  BL: "╚",
  BR: "╝",
  H: "═",
  V: "║",
};

const DIRECTION_INCREMENTS = [
  [0, -1],
  [1, -1],
  [1, 0],
  [1, 1],
  [0, 1],
  [-1, 1],
  [-1, 0],
  [-1, -1],
];

const COMMANDS = [
  "steps",
  "left",
  "right",
  "hover",
  "draw",
  "eraser",
  "coord",
  "render",
  "clear",
  "quit",
  "brush",
];

module.exports = {
  BRUSHES,
  BORDERS,
  DIRECTION_INCREMENTS,
  COMMANDS,
};
